'use strict';

const EJS = require('ejs');
const nodemailer = require('nodemailer');
const config = require('config');
const fs = require('fs');

const Mailer = module.exports = {

  /**
   * Nodemailer transport instance
   * @private
   * @type {Object}
   */
  _transport: null,

  /**
   * Render template with given params
   * @param  {string} templatePath
   * @param  {Object} params
   * @return {string}
   */
  renderTemplate (templatePath, params) {
    return EJS.render(fs.readFileSync(templatePath, 'utf-8'), params || {});
  },

  /**
   * Create a new transport for modemailer
   * @return {Object}
   */
  getTransport () {
    if (!Mailer._transport) {
      Mailer._transport = nodemailer.createTransport(config.transport);
    }
    return Mailer._transport;
  },

  /**
   * Will send a new email
   * @param  {Object} options
   * @param  {string} [template]
   * @param  {Object} [params]
   * @return {Promise}
   */
  send (options, template, params) {
    params = params || {};
    // Check template
    if (template) {
      options.html = Mailer.renderTemplate(template, params);
    }
    return new Promise((resolve, reject) => {
      Mailer
        .getTransport()
        .sendMail(options, (err, info) => {
          if (err) {
            return reject(err);
          }
          return resolve(info);
        });
    });
  }
};
