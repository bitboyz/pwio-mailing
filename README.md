# PW.io mailing module

This module is created for sending emails to users.
It has configured transports for `development`, `production`, `test`

## Installation

This is private module so. you couldn't install it from npm directly.

User this line to install it:

```bash
npm i --save git+https://bitboyz:HgxbulhwTAg3waN9oxPC3o3Ts5rofj3p@bitbucket.org:bitboyz/pwio-mailing.git
```
Or this line in your `package.json` file.

```
  "pwio-mailing": "git+https://bitboyz:HgxbulhwTAg3waN9oxPC3o3Ts5rofj3p@bitbucket.org:bitboyz/pwio-mailing.git"
```

Please note. you have to have permissions to perform this operation !

## Usage

Example:

```javascript
'use strict';

const Mailer = require('pwio-mailer');
const path = require('path');

const MailingOptions = {
  from: 'Fred Foo 👥 <foo@blurdybloop.com>', // sender address
  to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
  subject: 'Hello ✔', // Subject line
};

// This params will be binded to template variables
const templateParams = {
  username: 'Somebody',
  title: 'Mr.'
};

Mailer
  .send(MailingOptions, path.resolve(__dirname, '../templates/emailTemplate.ejs'), templateParams)
  .then((info) => {
    // all ok. email sent
  })
  .catch((err) => {
    // do something with error
  })

```
