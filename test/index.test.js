'use strict';

const expect = require('chai').expect;
const Mailer = require('../index');
const path = require('path');

describe('Mailer tests', () => {

  const EJS_TEMPLATE = path.resolve(__dirname, 'template/test.ejs');
  const MAIL_OPTIONS = {
    from: 'Fred Foo 👥 <foo@example.com>', // sender address
    to: 'bar@example.com', // list of receivers
    subject: 'Hello ✔', // Subject line
  };

  it('should bind template correctly', () => {
    const result = Mailer.renderTemplate(EJS_TEMPLATE, { title: 'test' });
    expect(result).to.contain('hello test');
  });

  it('should bind template on sending message', (done) => {
    Mailer
      .send(MAIL_OPTIONS, EJS_TEMPLATE, { title: 'test' })
      .then((info) => {
        expect(info).to.be.an('object');
        expect(info.envelope).to.be.an('object');
        expect(MAIL_OPTIONS.from).to.contain(info.envelope.from);
        expect(MAIL_OPTIONS.to).to.contain(info.envelope.to);
      })
      .then(done)
      .catch(done);
  });
});
