'use strict';

const sendmailTransport = require('nodemailer-sendmail-transport');

module.exports = {

  transport: sendmailTransport()

};
