'use strict';
const stubTransport = require('nodemailer-stub-transport');

module.exports = {

  transport: stubTransport()

};
